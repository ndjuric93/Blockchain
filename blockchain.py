import datetime as date

from block import Block

def create_genesis_block():
    return Block(0, date.datetime.now(), {
        "proof-of-work" : 9,
        "transactions" : None
    }, "0")

blockchain = [create_genesis_block()]
previous_block = blockchain[0]


