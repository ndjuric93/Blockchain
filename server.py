import json
import datetime as date

from flask import Flask
from flask import request

from block import Block
import blockchain 

node = Flask(__name__)
this_nodes_transactions = []

@node.route('/txion', methods=['POST'])
def transaction():
    if request.method == 'POST':
        new_txion = request.get_json()
        this_nodes_transactions.append(new_txion)
        print "New transaction!"
        print "FROM: {}".format(new_txion['from'])
        print "TO: {}".format(new_txion['to'])
        print "AMOUNT: {}".format(new_txion['amount'])
        return "Transaction submission successful\n"

def proof_of_work(last_proof):
    incrementor = last_proof + 1
    while not (incrementor % 9 == 0 and incrementor % last_proof == 0):
        incrementor += 1
    return incrementor

miner_address = "q3nf394hjg-random-miner-address-34nf3i4nflkn3oi"

@node.route('/mine', methods=['GET'])
def mine():
    last_block = blockchain.blockchain[len(blockchain.blockchain) - 1]
    last_proof = last_block.data['proof-of-work']
    proof = proof_of_work(last_proof)
    this_nodes_transactions.append({
        'from' : 'network',
        'to' : miner_address,
        'amount' : 1
    })
    new_block_data = {
        'proof-of-work' : proof,
        'transactions' : list(this_nodes_transactions)
    }
    new_block_index = last_block.index + 1
    new_block_timestamp = this_timestamp = date.datetime.now()
    last_block_hash = last_block.hash
    this_nodes_transactions[:] = []

    mined_block = blockchain.Block(
            new_block_index,
            new_block_timestamp,
            new_block_data,
            last_block_hash)
    blockchain.blockchain.append(mined_block)

    return json.dumps({
        'index' : new_block_index,
        'timestamp' : str(new_block_timestamp),
        'data' : new_block_data,
        'hash' : last_block_hash
    }) + '\n' 

@node.route('/blocks', methods=['GET'])
def get_blocks():
    chain_to_send = [{
        "index" : str(block.index),
        "timestamp" : str(block.timestamp),
        "data" : str(block.data),
        "hash" : block.hash
    } for block in blockchain.blockchain] 
    return json.dumps(chain_to_send)

def find_new_chains():
    return [json.loads(requests.get(node_url + '/blocks').content) for node_url in peer_nodes]

def consensus():
    other_chains = find_new_chains()
    longest_chain = blockchain.blockchain
    longest_chain = max(other_chains, lambda chain : len(chain))
    
node.run()

